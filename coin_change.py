# https://leetcode.com/problems/coin-change/
class Solution:
    def coinChange(self, coins: list[int], amount: int) -> int:
        result = making_change(coins, amount)
        return result


def making_change(coins, amount):
    if amount == 0:
        return 0

    coin_change_dict = {}

    for x in coins:
        current = x
        i = 1
        temp_dict = {0: 0}
        while current <= amount:
            temp_dict[current] = i
            i += 1
            current += x

        update_coin_dict = {}

        if coin_change_dict:
            for y in coin_change_dict:
                for z in temp_dict:
                    if y + z > amount:
                        continue
                    if y + z in coin_change_dict:
                        if coin_change_dict[y + z] > coin_change_dict[y] + temp_dict[z]:
                            coin_change_dict[y + z] = coin_change_dict[y] + temp_dict[z]
                    elif y + z in update_coin_dict:
                        if update_coin_dict[y + z] > coin_change_dict[y] + temp_dict[z]:
                            update_coin_dict[y + z] = coin_change_dict[y] + temp_dict[z]
                    else:
                        update_coin_dict[y + z] = coin_change_dict[y] + temp_dict[z]
            coin_change_dict.update(update_coin_dict)
        else:
            coin_change_dict.update(temp_dict)

    if amount in coin_change_dict:
        return coin_change_dict[amount]

    return -1


def making_change_list_view(coins, amount):
    if amount == 0:
        return 0

    coin_change_dict = {}

    for x in coins:
        num_of_coins = [0] * len(coins)
        current = x
        i = 1
        temp_dict = {0: num_of_coins}
        while current <= amount:
            num_of_coins = [0] * len(coins)
            num_of_coins[coins.index(x)] = i
            temp_dict[current] = num_of_coins

            i += 1
            current += x

        update_coin_dict = {}

        if coin_change_dict:
            for y in coin_change_dict:
                for z in temp_dict:
                    if y + z > amount:
                        continue
                    if y + z in coin_change_dict:
                        if sum(coin_change_dict[y + z]) > sum(coin_change_dict[y]) + sum(temp_dict[z]):
                            coin_change_dict[y + z] = [first + second for first, second in zip(coin_change_dict[y],
                                                                                               temp_dict[z])]
                    else:
                        update_coin_dict[y + z] = [first + second for first, second in zip(coin_change_dict[y],
                                                                                           temp_dict[z])]
            coin_change_dict.update(update_coin_dict)
        else:
            coin_change_dict = temp_dict

    if amount in coin_change_dict:
        return coin_change_dict[amount]

    return -1

# def making_change_wrong(coins, amount):
#     if amount == 0:
#         return 0
#
#     for i in range(len(coins)):
#         if coins[i] <= amount:
#             num_of_coin[i] += 1
#             result = making_change(coins, amount - coins[i])
#             if result == -1:
#                 break
#             return 1 + result
#
#     return -1
