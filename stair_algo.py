def stair_algo_func(num_of_steps):
    if num_of_steps == 2:
        return 1, 2
    prev2, prev = stair_algo_func(num_of_steps - 1)

    current = prev + prev2
    return prev, current

def stairs_algo_func_it(num_of_steps):
    if num_of_steps < 4:
        return num_of_steps

    prev2, prev = 2, 3

    for i in range(4, num_of_steps + 1):
        current = prev + prev2
        prev2, prev = prev, current

    return prev, current




