# https://leetcode.com/problems/climbing-stairs/
class Solution:
    def climbStairs(self, n: int) -> int:
        if n < 4:
            return n

        prev2, prev = 2, 3

        for i in range(4, n + 1):
            current = prev + prev2
            prev2, prev = prev, current

        return current
