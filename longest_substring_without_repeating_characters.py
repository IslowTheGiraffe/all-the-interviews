# https://leetcode.com/problems/longest-substring-without-repeating-characters/
class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        cnt = 0
        high_num = 0
        pointer = 0
        dictionary = {}

        # Checking each letter in the string
        for index, letter in enumerate(s.lower()):
            # Checking if the letter is in the set
            if (letter in dictionary) and (dictionary[letter] >= pointer):
                if cnt > high_num:
                    high_num = cnt
                cnt = index - dictionary[letter]
                pointer = dictionary[letter] + 1
            else:
                cnt += 1
        dictionary[letter] = index

        return max(cnt,high_num)

        start = 0
        res = 0
        dic = {}
        for i, c in enumerate(s):
            if c in dic and start <= dic[c]:
                start = dic[c] + 1
            else:
                res = max(res, i - start + 1)
            dic[c] = i
        return res
