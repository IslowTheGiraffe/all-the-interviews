class Solution:
    # https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
    def maxProfit(self, prices: list[int]) -> int:
        low_num = prices[0]
        max_profit = 0

        for num in prices:
            low_num = min(num, low_num)
            current_num = num - low_num
            max_profit = max(current_num, max_profit)

        return max_profit

    # This didn't work as expected
    def maxProfit2(self, prices: list[int]) -> int:

        if len(prices) == 2:
            return prices[1] - prices[0]

        if len(prices) == 3:
            part1 = prices[1] - prices[0]
            part2 = prices[2] - prices[1]
            part3 = prices[2] - prices[0]
            max_profit = max(part1, part2, part3)
            if max_profit > -1:
                return max_profit
            else:
                return 0

        half_of_price = int(len(prices) / 2)

        half1 = self.maxProfit(prices[0:half_of_price])
        half2 = self.maxProfit(prices[half_of_price:len(prices)])

        min_left = 0
        max_right = 0
        for x in prices[0:half_of_price]:
            if x < min_left:
                min_left = x
        for x in prices[half_of_price:len(prices)]:
            if x > max_right:
                max_right = x

        best_value = max_right - min_left


        max_profit = max(half1, half2, best_value)

        if max_profit > -1:
            return max_profit
        else:
            return 0
