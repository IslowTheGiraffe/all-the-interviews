class Solution:
    def two_sum(self, nums: list[int], target: int) -> list[int]:
        for x in range(len(nums)):
            for y in range(len(nums)):
                if nums[x] == nums[y]:
                    continue
                if nums[x] + nums[y] == target:
                    return [x, y]

    def fast_two_sum(self, nums: list[int], target: int) -> list[int]:
        num_dict = {}

        for x in range(len(nums)):
            diff = target - nums[x]
            if diff in num_dict:
                return [x, num_dict[diff]]
            else:
                # print("Adding " + str(nums[x]) + " at index " + str(x))
                num_dict[nums[x]] = x

